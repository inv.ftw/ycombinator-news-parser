task :parse_posts do
  require 'open-uri'
  require 'net/http'

  pages_count = 5
  user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_0) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.854.0 Safari/535.2'
  base_url = 'https://news.ycombinator.com/?p='

  (1..pages_count).each do |page_number|
    doc = Nokogiri::HTML(open(base_url+page_number.to_s, 'User-Agent' => user_agent, 'read_timeout' => '10' ), nil, 'UTF-8')
    doc.css('tr.athing').each do |node|
      Post.add_record(node)
    end

    sleep_time = rand(10) + 1
    sleep(sleep_time)
  end
end