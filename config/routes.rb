Rails.application.routes.draw do
  root 'posts#index'
  resources :posts, only: [:index, :show] do
    collection do
      get 'parse'
    end
  end
end
