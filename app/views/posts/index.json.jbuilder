json.array!(@posts) do |post|
  json.extract! post, :id, :title, :author, :url
  json.url post_url(post, format: :json)
end
