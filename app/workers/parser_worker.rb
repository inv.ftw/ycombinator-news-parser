class ParserWorker
  require 'rake'
  include Sidekiq::Worker

  def perform
    Rake::Task.clear
    YcombinatorNewsParser::Application.load_tasks
    Rake::Task[:parse_posts].reenable
    Rake::Task[:parse_posts].invoke
  end
end