class Post < ActiveRecord::Base
  validates_uniqueness_of :url
  paginates_per 15

  def self.add_record(node)
    link_with_title = node.at_css('.title a')
    author = node.next.at_css('.subtext a').text.strip
    new(title: link_with_title.text, url: link_with_title['href'], author: author).save
  end
end
